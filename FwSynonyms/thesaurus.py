﻿import json, os, re
import itertools
import sqlite3
from collections import defaultdict, OrderedDict, Counter
from operator import itemgetter
from FwText import word_tokenize, remove_stopwords, all_ngrams_tuples
from FwFreq import freq, unique

__all__ = ["FwSynonyms",]


def _data_full_path(path_inside_data):
    '''
    @description: concatenates the path of FwSynonyms with /data/
    and the path of a vocabulary file.
    @arg path_inside_data: {str}
    @return: {str}
    '''
    return os.path.join(os.path.dirname(__file__),
                        "data",
                        path_inside_data)


re_LETTER = re.compile(r"[A-Z]", flags=re.I)


class OrderedCounter(Counter, OrderedDict):
    'Counter that remembers the order elements are first seen'
    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__,
                            OrderedDict(self))
    def __reduce__(self):
        return self.__class__, (OrderedDict(self),)


def list_index(a_list, value, default_index=None):
    if value not in a_list: return default_index
    return a_list.index(value)


class FwSynonyms():
    ''' This is a class meant to find synonyms using a thesaurus '''
    def __init__(self, i_max_syns_per_word=-1):
        self._words, self._ids = None, None
        self.max_syns_per_word = i_max_syns_per_word
        self._load_dicts()
        
    def __enter__(self):
        self._load_dicts()
        
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
    
    def _load_dicts(self):
        db = sqlite3.connect(_data_full_path("thesaurus_synonyms.sqlite"), uri=True)
        
        res = db.execute("select {} from {}".format(', '.join(['word', 'word_id', 'synonyms']), "words"))
        a_word_table = [row for row in res]
        if self.max_syns_per_word > 0:
            self._words = {
                tuple(word.split()): (word_id, json.loads(synonyms)[:self.max_syns_per_word])
                for word, word_id, synonyms in a_word_table
            }
        else:
            self._words = {
                tuple(word.split()): (word_id, json.loads(synonyms))
                for word, word_id, synonyms in a_word_table
            }
        a_word_table = None
        res.close()
        
        self._ids = {
            word_id: word
            for word, (word_id, synsets) in self._words.items()
        }
        #print("{} words in synonym dict".format(len(self._words)))
        
    def close(self):
        self._words = None
        self._ids = None
    
    def word_synonyms(self, word, b_as_tuples=False):
        ''' get the synonyms for a word '''
        t_word = self._get_word_row(word)
        if not t_word: return []
        i_word_id, a_syns = t_word
        a_synonyms = [self._ids.get(syn_id) for syn_id in a_syns]
        
        if not b_as_tuples:
            a_synonyms = [' '.join(t_syn) for t_syn in a_synonyms if t_syn]
            
        return a_synonyms
    
    def synonyms(self, words, b_as_tuples=False, b_as_freqs=False):
        ''' get the synonyms
            if the input is a group words, return the synonyms in common between them '''
        if not words:
            return {} if b_as_freqs else []
        elif isinstance(words, (str, tuple)) and isinstance(words[0], str):
            a_syns = self.word_synonyms(words, b_as_tuples=b_as_tuples)
            if b_as_freqs:
                return freq(a_syns)
            else:
                return a_syns
            
        elif isinstance(words, (list, set)):
            a_syns = [
                self.word_synonyms(word, b_as_tuples=b_as_tuples)
                for word in words
            ]
            e_syns = freq(a_syns)
            if b_as_freqs:
                return e_syns
            else:
                return [syn for syn, ct in e_syns.items() if ct >= len(words)]
    
    def _get_word_row(self, word):
        if isinstance(word, str):
            t_word = tuple(word.split())
        elif isinstance(word, int):
            t_word = self.ids_words.get(word_id)
        elif isinstance(word, list):
            t_word = tuple(word)
        else:
            t_word = word
        
        return self._words.get(t_word)
    
    def synonyms_in_text(self, text, ngrams=None, b_as_tuples=False):
        ''' look for synonyms that appear together in a text'''
        
        if isinstance(text, int):
            return []
        elif isinstance(text, str):
            a_text = word_tokenize(text.lower())
        else:
            a_text = remove_stopwords([word.lower() for word in text])
            
        if not a_text: return []
        
        a_words = unique(a_text)  # unique title words in order
        
        # ngrams are necessary to find synonym phrases
        if isinstance(ngrams, (set, list, tuple)):
            #ngrams = ngrams
            e_freqs = {ngram: 1 for ngram in ngrams}
        elif ngrams == False:
            #ngrams = set()
            e_freqs = {}
        elif ngrams is None:
            ngrams = all_ngrams_tuples(a_text)
            e_freqs = freq(ngrams)
        elif isinstance(ngrams, dict):
            #ngrams = set(ngrams.keys())
            if all(isinstance(val, int) for val in ngrams):
                e_freqs = ngrams
            else:
                e_freqs = {ngram: 1 for ngram in ngrams.keys()}
        else:
            e_freqs = freq(ngrams)
            #ngrams = set(ngrams)
        
        e_syn_sets = defaultdict(OrderedCounter)  # groups of synonyms of each other
        e_syn_to_set = defaultdict(OrderedCounter)  # which group each synonym in
        i_syn_set_id = 0
        
        for s_word in a_words:  # loop through unique words
            if len(s_word) < 3 or not re_LETTER.search(s_word): continue
            
            t_word = tuple(s_word.split())
            t_word_row = self._words.get(t_word)
            
            if not t_word_row: continue
            i_word_id, a_syns = t_word_row
            
            if not e_syn_to_set.get(t_word):
                i_syn_set_id = len(e_syn_to_set) + 1
                e_syn_sets[i_syn_set_id][t_word] += 1
                e_syn_to_set[t_word][i_syn_set_id] += 1
            
            for i_syn_id in a_syns:  # loop through the possible synonyms to see if they appear in the title
                t_syn = self._ids.get(i_syn_id)
                s_syn = ' '.join(t_syn)
                i_ct = e_freqs.get(t_syn, 0)
                if i_syn_id == i_word_id: i_ct -= 1
                    
                if i_ct > 0 and len(s_syn) >= 3 and re_LETTER.search(s_syn):
                    for i_synset in e_syn_to_set[t_word]:
                        e_syn_sets[i_synset][t_syn] += 1
                        e_syn_to_set[t_syn][i_synset] += 1
                
                    for i_synset, c_syns in e_syn_sets.items():
                        if t_syn in c_syns and t_word not in c_syns:
                            e_syn_to_set[t_word][i_synset] += 1
                            e_syn_sets[i_synset][t_word] += 1
                    
        #print('e_syn_sets', e_syn_sets)
        a_syn_sets = [
            (tuple(e_syns.keys()), len(e_syns))
            for i_synset, e_syns in e_syn_sets.items()
            if len(e_syns) > 1]
        a_syn_sets.sort(key=itemgetter(1), reverse=True)
        
        a_synonyms = []
        a_used = []
        
        for t_synset, i_syns in a_syn_sets:
            if any(all(word in c_set for word in t_synset) for c_set in a_used):
                continue
            a_used.append(t_synset)
            
            a_set = []
            for t_syn in t_synset:
                i_ct = e_freqs.get(t_syn, 1)
                if b_as_tuples:
                    a_set.extend([t_syn] * i_ct)
                else:
                    a_set.extend([' '.join(t_syn)] * i_ct)
                    
            if len(a_set) == 1: continue
            a_synonyms.append(a_set)
        
        return a_synonyms
    
    def synonym_similarity(self, a_synonyms, b_absolute=False):
        ''' Measure the similarity between a list of synonyms '''

        e_syn_lists = {}
        #a_distances = []
        distance, i_count = 0, 0

        for syn1, syn2 in itertools.combinations(set(a_synonyms), 2):
            if syn1 in e_syn_lists:
                syn1_list = self.word_synonyms(syn1)
            else:
                syn1_list = e_syn_lists[syn1] = self.word_synonyms(syn1)

            if syn2 in e_syn_lists:
                syn2_list = self.word_synonyms(syn2)
            else:
                syn2_list = e_syn_lists[syn2] = self.word_synonyms(syn2)

            i_len1 = len(syn1_list) * 2
            i_len2 = len(syn2_list) * 2
            i_syn2_idx = list_index(syn1_list, syn2, default_index=i_len1)
            i_syn1_idx = list_index(syn2_list, syn1, default_index=i_len2)

            i_count += 2
            if b_absolute:
                distance += i_syn2_idx
                distance += i_syn1_idx
            else:
                distance += 1 - (i_syn2_idx / i_len1)
                distance += 1 - (i_syn1_idx / i_len2)

            #a_distances.append((i_syn2_idx, len(syn1_list), 1 - (i_syn2_idx / i_len1)))
            #a_distances.append((i_syn1_idx, len(syn2_list), 1 - (i_syn1_idx / i_len2)))

        #print(distance, i_count)
        d_similarity = (distance / i_count)

        c_possible_syns = set(syn for a_syns in e_syn_lists.values() for syn in a_syns)
        i_synset_size = len(c_possible_syns)

        return d_similarity, i_synset_size
