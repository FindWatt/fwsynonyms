﻿'''
Test cases for FwSynonyms/thesaurus
'''
from __future__ import absolute_import
from os import path
from FwText import word_tokenize
from FwFile import folder_from_path, path_exists, file_exists
from FwSynonyms import thesaurus


class TestDataPath():
    folder = folder_from_path(thesaurus._data_full_path('thesaurus.sqlite'))
    def test_folder_exists(self):
        assert path_exists(self.folder)

    def test_files_present(self):
        a_files = [
            'thesaurus.sqlite',
            'thesaurus_synonyms.sqlite',
        ]
        for s_file in a_files:
            assert file_exists(path.join(self.folder, s_file))


class TestWordSynonyms():
    syns = thesaurus.FwSynonyms(i_max_syns_per_word=10)
    def test_blank(self):
        assert self.syns.word_synonyms('') == []

    def test_word(self):
        synonyms = ['puppy', 'cur', 'stray', 'tyke', 'bitch', 'mutt', 'hound', 'mongrel', 'pup', 'doggy']
        assert self.syns.word_synonyms('dog') == synonyms

    def test_phrase(self):
        synonyms = ['wading', 'wade', 'subscribes', 'subscribe']
        assert self.syns.word_synonyms(("pitch", "in")) == synonyms


class TestSynonyms():
    syns = thesaurus.FwSynonyms(i_max_syns_per_word=-1)
    def test_blank(self):
        assert self.syns.synonyms('') == []
        assert self.syns.synonyms([]) == []
        assert self.syns.synonyms(('',)) == []
        assert self.syns.synonyms(('',)) == []

    def test_word(self):
        synonyms = [
            'powerful', 'effective', 'significant', 'potent',
            'logical', 'valid', 'telling', 'impactful',
            'forceful', 'analytical', 'convincing', 'fluent',
            'emphatic', 'satisfactory', 'persuasive','puncher',
             'forcible']
        assert self.syns.word_synonyms('cogent') == synonyms
    
    def test_common(self):
        synonyms = ['subset', 'subsets', 'factored']
        assert self.syns.synonyms(['cut', 'share', 'ration']) == synonyms

    def test_freqs(self):
        synonyms = {
            'alluring': 1, 'analytical': 1, 'believable': 1, 'cogent': 1,
            'compelling': 1, 'conclusive': 1, 'convincing': 2, 'credible': 1,
            'effective': 1, 'effectual': 1, 'efficacious': 1, 'efficient': 1,
            'eloquent': 1, 'emphatic': 1, 'energetic': 1, 'fluent': 2,
            'forceful': 2,  'forcible': 2, 'impactful': 1,'impressive': 1,
            'influential': 1,  'inspiring': 1, 'inviting': 1,'logical': 2,
            'moving': 1,  'persuasive': 1, 'plausible': 1,'potent': 2,
            'powerful': 2,  'puncher': 2, 'satisfactory': 1,'significant': 1,
            'telling': 1,  'urgent': 1,'valid': 2, 'vigorous': 1}
        assert self.syns.synonyms(['cogent', 'persuasive'], b_as_freqs=True) == synonyms


class TestSynonymsInText():
    syns = thesaurus.FwSynonyms()
    def test_blank(self):
        assert self.syns.synonyms_in_text('') == []

    def test_sets(self):
        s_text = "Take a peek, glance or a fleeting look"
        synonyms = [['peek', 'glance', 'look', 'fleeting look']]
        assert self.syns.synonyms_in_text(s_text) == synonyms

    def test_multiple_sets(self):
        s_text = "Celebrate Mother Day by Giving Your Mama a World's Best Mom Mug Cup for hot and warm drinks"
        synonyms = [['mother', 'mom', 'mama'], ['mug', 'cup', 'drinks'], ['hot', 'warm']]
        assert self.syns.synonyms_in_text(s_text) == synonyms

    def test_return_tuples(self):
        a_text = word_tokenize("Take a peek, glance or a fleeting look")
        synonyms = [[('peek',), ('glance',), ('look',), ('fleeting', 'look')]]
        assert self.syns.synonyms_in_text(a_text, b_as_tuples=True) == synonyms
