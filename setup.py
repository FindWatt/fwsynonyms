from os import path, system
import setuptools
from setuptools.command.install import install
import re, sys

module_path = path.join(path.dirname(__file__), 'FwSynonyms/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall("\d+\.\d+\.\d+", version_line)[0]


class CustomInstall(install):
    """
    @description: Installs external FindWatt dependencies before installing
    the current package.
    """
    def run(self):
        self.install_fw_deps()
        install.run(self)

    @staticmethod
    def install_fw_deps():
        if sys.version_info.major == 2:
            system("pip install git+https://bitbucket.org/FindWatt/fwtext")
            system("pip install git+https://bitbucket.org/FindWatt/fwfreq")
            system("pip install git+https://bitbucket.org/FindWatt/fwfile")
        elif sys.version_info.major == 3:
            system("pip3 install git+https://bitbucket.org/FindWatt/fwfile")
            system("pip3 install git+https://bitbucket.org/FindWatt/fwfreq")
            system("pip3 install git+https://bitbucket.org/FindWatt/fwfile")


setuptools.setup(
    name="FwSynonyms",
    version=__version__,
    url="https://bitbucket.org/FwPyClassification/fwsynonyms",

    author="FindWatt",

    description="Classes to find synonyms for word, or appearing together in text.",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={'': ["data/*.sqlite"]},
    py_modules=['FwSynonyms'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "pytest",
        "pytest-cov",
        "pylint",
    ],
    cmdclass={'install': CustomInstall}
)
