Contains classes that implement synonym related methods

Install with:

```
#!bash

pip install git+https://bitbucket.org/FindWatt/fwsynonyms
```


Usage:
```
#!python

import FwSynonyms

# FwSynonyms.FwSynonyms is for finding synonyms for a word, of the synonym sets within a piece of text.
# Initialize it using the default vocab file
o_synonyms = FwSynonyms.FwSynonyms()
# Optionally, you can limit the number of synonyms looked for to the X best matches for each word
o_synonyms = FwSynonyms.FwSynonyms(i_max_syns_per_word=10)

# Get a list of synonyms for a given word
o_synonyms.word_synonyms("dog") in vocab
>> ['puppy', 'cur', 'stray', 'tyke', 'bitch', 'mutt', 'hound', 'mongrel']

# Get a list of synonyms for a phrase
o_syns.word_synonyms(("pitch", "in"))
>> ['wading', 'wade', 'subscribes', 'subscribe']

# Getting the synonyms that appear together in a piece of text
o_syns.synonyms_in_text("Celebrate Mother Day by Giving Your Mama a World's Best Mom Mug Cup for hot and warm drinks")
>> [['mama', 'mom', 'mother'], ['cup', 'mug', 'drinks'], ['hot', 'warm']]


# You can pass in pre-tokenized text
a_text = FwText.word_tokenize("Take a peek, glance or a fleeting look")
o_syns.synonyms_in_text(a_text)
>> [['look', 'glance', 'peek', 'fleeting look']]

# For both word_synonyms and synonyms_in_text, you can have synonyms returned as tuples instead of strings
a_text = FwText.word_tokenize("Take a peek, glance or a fleeting look")
o_syns.synonyms_in_text(a_text, b_as_tuples=True)
>> [[('look',), ('glance',), ('peek',), ('fleeting', 'look')]]

